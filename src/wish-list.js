import { LitElement, html } from 'lit-element';

export class WishList extends LitElement {

    static get properties() {
        return {
            increment: { type: Number, attribute: false },
            wishList: { type: Array }
        }
    }

    constructor() {
        super();

        this.increment = 0;
        this.wishList = [
            {
                id: ++this.increment,
                content: 'Travel to the moon'
            }, 
            {
                id: ++this.increment,
                content: 'Pay de Gym'
            },
            {
                id: ++this.increment,
                content: 'Go to the gym'
            }
        ];
    }

    __addWish(e) {
        e.preventDefault();
        
        let $inputWish = this.shadowRoot.querySelector('#input-wish');
        let value = $inputWish.value.trim();

        if (value === '') return null;

        this.wishList.push({
            id: ++this.increment,
            content: value
        });

        e.target.reset();
        $inputWish.focus();

        this.requestUpdate();
    }

    __archiveDone() {
        const $wishList = this.shadowRoot.querySelectorAll('.check-wish');
        let newWishList = this.wishList;

        $wishList.forEach(($wish) => {
            if ($wish.checked) {
                let indexWish = newWishList.findIndex((wish) => wish.id == $wish.value);
                newWishList.splice(indexWish, 1);
            }
        });

        this.wishList = newWishList;
        this.requestUpdate();
    }

    __renderWishes() {
        return html`
            <ul>
                ${this.wishList.map((wish) => 
                html`
                <li>
                    <label>
                        <input type="checkbox" class="check-wish" value="${ wish.id }"> ${ wish.content }
                    </label>
                </li>`)}
            </ul>
        `;
    }

    render() {
        return html`
            <h1>My wishlist</h1>
            <form @submit="${this.__addWish}">
                <fieldset>
                    <legend>New wish</legend>
                    <input type="text" id="input-wish">
                    <button type="submit">Add Wish</button>
                </fieldset>
            </form>
            ${this.__renderWishes()}
            <button @click="${this.__archiveDone}">Archive done</button>
        `;
    }

    updated() {
        const $wishList = this.shadowRoot.querySelectorAll('.check-wish');
        $wishList.forEach(($wish) => {
            $wish.checked = false;
        });
    }

}

customElements.define('wish-list', WishList);